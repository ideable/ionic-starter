'use strict';
angular.module('main')
  .controller('UserCtrl', function ($log, $state, User) {

    var ctrl = this;
    ctrl.credentials = {
      email: 'postman', //Sample
      password: 'password' //Sample
    };

    // tries to sign the user up and displays the result in the UI
    this.signup = function () {
      $log.log('UserCtrl', 'signup');

      //$state.go('user.signup');
    };

    // tries to sign in the user and displays the result in the UI
    this.signin = function () {
      $log.log('UserCtrl', 'signin');

      User.login(ctrl.credentials)
        .then(function (user) {
          $log.log('UserCtrl', 'Usuario logeado', user);

          $state.go('main.list');
        });

    };
  });
