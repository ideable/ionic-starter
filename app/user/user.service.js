'use strict';
angular.module('main')
  .service('User', function ($log, Auth) {

    $log.log('Service', 'User');

    var modules = {
      login: Auth.login,
      logout: Auth.logout,
      signup: Auth.signup
    };


    return modules;
  });
