'use strict';

angular.module('main')
.config(function ($stateProvider) {

  $stateProvider
    .state('user', {
      url: '/user',
      abstract: true,
      templateUrl: 'user/user.html'
    })
    .state('user.login', {
      url: '/login',
      views: {
        'pageContent': {
          templateUrl: 'user/login/login.html',
          controller: 'UserCtrl as ctrl'
        }
      }
    });
});
