'use strict';
angular.module('main')
.controller('MenuCtrl', function ($log, $scope, $translate) {

  $log.log('Hello from your Controller: MenuCtrl in module main:. This is your controller:', this);

  $scope.changeLanguage = function (key) {
    $translate.use(key);
  };

});
