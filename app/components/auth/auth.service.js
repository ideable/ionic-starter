'use strict';
angular.module('main')
.service('Auth', function ($log, $http, $q, Config) {

  $log.log('Service', 'Auth');

  var currentUser;
  var token;

  var modules = {
    login: login,
    logout: logout,
    signup: signup,
    getCurrentUser: getCurrentUser,
    getToken: getToken
  };

  function setUserToken(user) {
    currentUser = user;
    token = user.token;

    return $q.resolve(user);
  }

  function login(credentials) {
    credentials = credentials || {};


    /*
    // TODO Implement
    var url = Config.ENV.SERVER_URL + '/users/login';
    return $http
      .post(url, {
        email: credentials.email,
        password: credentials.password
      })
      .then(setUserToken);*/

    $log.debug(credentials);
    var fakeUser = {
      name: 'John',
      lastname: 'Doe',
      token: 'cG9zdG1hbjpwYXNzd29yZA=='
    };

    return $q.resolve(fakeUser)
      .then(setUserToken);
  }

  function logout() {
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('user');
  }

  function signup(user) {
    var url = Config.ENV.SERVER_URL + '/users';

    return $http.post(url, user);
  }

  function getCurrentUser() {
    return currentUser;
  }

  function getToken() {
    return token;
  }


  return modules;
});
