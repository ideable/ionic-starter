'use strict';
angular.module('App', [
  // load your modules here
  'main'
])
  .config(appConfig);

function appConfig($ionicConfigProvider, $httpProvider, $translateProvider) {
  $ionicConfigProvider.backButton.text('').icon('ion-chevron-left').previousTitleText(false);
  $ionicConfigProvider.tabs.position('top');

  // Auth interceptor
  $httpProvider.interceptors.push('authInterceptor');

  //HTML5 url
  //$locationProvider.html5Mode(true);

  // No sanitization strategy has been configured. This can have serious security implications. See http://angular-translate.github.io/docs/#/guide/19_security for details.
  $translateProvider.useSanitizeValueStrategy('sanitize');
}
