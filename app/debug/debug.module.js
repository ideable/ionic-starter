'use strict';

angular.module('main')
.config(function ($stateProvider) {

  $stateProvider
    .state('main.debug', {
      url: '/debug',
      views: {
        'pageContent': {
          templateUrl: 'debug/debug.html',
          controller: 'DebugCtrl as ctrl'
        }
      }
    });
});
