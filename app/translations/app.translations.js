'use strict';
angular.module('main')
  .config(function ($translateProvider) {
    $translateProvider.translations('es', {
      title: 'Hola',
      foo: 'Esto es un párrafo',
      button_es: 'Cambiar a español',
      button_eus: 'Cambiar a euskera'
    });

    $translateProvider.translations('eus', {
      title: 'Kaixo',
      foo: 'Hauxe parrafoa da',
      button_es: 'Gaztelaniaz',
      button_eus: 'Euskaraz'
    });

    $translateProvider.preferredLanguage('es');
  });
