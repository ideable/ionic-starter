'use strict';
angular.module('App')
  .factory('authInterceptor', function ($rootScope, $q, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if (window.localStorage.getItem('token')) {
          config.headers.Authorization = 'Bearer ' + JSON.parse(window.localStorage.getItem('token'));
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function (response) {
        if (response.status === 401) {
          $location.path('/user/login');

          // remove any stale tokens
          window.localStorage.removeItem('token');
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  });
